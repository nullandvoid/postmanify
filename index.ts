import axios, { AxiosError, AxiosResponse, AxiosRequestConfig } from 'axios';
import { ApiItem, ApiNode, ApiUrl, MethodOptions, PostmanCollection, PostmanifyAPI, PostmanifyOptions, PostmanItem, PostmanQuery, PostmanURL, PostmanVariable } from './postmanifyTypes';

let urlVariables: { [key: string]: string } | undefined = {};
let middleware: undefined | ((middlewareOpts: { [key: string]: any }, res: AxiosResponse<any> | undefined, error?: AxiosError) => any);

const postmanifiy = (postmanCollection: PostmanCollection, opts?: PostmanifyOptions): PostmanifyAPI => {
  urlVariables = opts?.urlVariables;
  middleware = opts?.middleware;
  return generateApiTree(postmanCollection.item);
};

const generateApiTree = (items: PostmanItem[]): ApiItem => {
  const node: ApiItem = {};
  const counter: { [method: string]: number } = {};
  const requestDetails: { [method: string]: ApiNode } = {};

  items.forEach((item: PostmanItem) => {
    if (item.item) {
      node[item.name.toLowerCase().replace(/\s/, '_')] = generateApiTree(item.item);
    } else if (item.request) {
      let name = item.name.toLowerCase();

      name = name.replace(/\s/g, '_');

      const request: ApiNode = {
        method: item.request.method.toLowerCase(),
        name,
        url: item.request ? item.request.url : '',
      };

      if (counter[item.request.method.toLowerCase()] === 1) {
        const previousItem = requestDetails[item.request.method.toLowerCase()];
        node[previousItem.name] = functionGenerator(previousItem);
        delete node[item.request.method.toLowerCase()];
        node[request.name] = functionGenerator(request);
      } else {
        node[item.request.method.toLowerCase()] = functionGenerator(request);
        requestDetails[item.request.method.toLowerCase()] = request;
        counter[item.request.method.toLowerCase()] = 1;
      }
    }
  });

  return node;
};

const parseUrl = (url: PostmanURL, methodOptions: MethodOptions): string => {
  let parsed: string = url.raw;
  const questionMarkPosition = parsed.indexOf('?');
  parsed = parsed.substring(0, questionMarkPosition != -1 ? questionMarkPosition : parsed.length);

  const variableMatches = parsed.match(/{{([^}]+)}}/g);
  const pathMatches = parsed.match(/\/:([^/]+)\/?/g);

  variableMatches?.forEach((variable: string) => {
    const key = variable.replace(/{{/, '').replace(/}}/, '');
    const regex: RegExp = new RegExp(`{{${key}}}`);

    if (methodOptions?.variables?.[key]) {
      parsed = parsed.replace(regex, methodOptions.variables[key]);
    } else if (urlVariables?.[key]) {
      parsed = parsed.replace(regex, urlVariables[key]);
    } else {
      throw new Error(`[Postmanify]: Postman Variable not supplied ${key}`);
    }
  });

  pathMatches?.forEach((variable: string) => {
    const key = variable.replace(/\/:/, '').replace(/\//, '');
    const regex: RegExp = new RegExp(`/:${key}`);

    if (methodOptions?.path?.[key]) {
      parsed = parsed.replace(regex, `/${methodOptions.path[key]}`);
    } else {
      throw new Error(`[Postmanify]: Postman path variable not supplied ${key}`);
    }
  });

  return parsed;
};

const functionGenerator = (request: ApiNode) => {
  const generatedFunction = async (methodOptions: MethodOptions) => {
    if (typeof request.url === 'string') {
      throw new Error(`[Postmanify]: Postman item has no URL ${request.name}`);
    }

    let returnValue: any;

    const config: AxiosRequestConfig = {
      headers: methodOptions?.headers,
      params: methodOptions?.query,
    };

    switch (request.method) {
      case 'post':
        returnValue = axios.post(parseUrl(request.url, methodOptions), methodOptions?.body, config);
        break;
      case 'get':
        returnValue = axios.get(parseUrl(request.url, methodOptions), config);
        break;
      case 'delete':
        returnValue = axios.delete(parseUrl(request.url, methodOptions), config);
        break;
      case 'put':
        returnValue = axios.put(parseUrl(request.url, methodOptions), methodOptions?.body, config);
        break;
      case 'patch':
        returnValue = axios.patch(parseUrl(request.url, methodOptions), methodOptions?.body, config);
        break;
      default:
        throw new Error(`[Postmanify]: unsupported method type ${request.method}`);
    }

    if (middleware) {
      let res: AxiosResponse;

      try {
        res = await returnValue;
      } catch (error) {
        return middleware(methodOptions?.middlewareOpts, undefined, error as AxiosError);
      }
      return middleware(methodOptions?.middlewareOpts, res);
    }

    return returnValue;
  };

  return generatedFunction;
};

export default postmanifiy;
