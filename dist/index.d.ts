import { PostmanCollection, PostmanifyAPI, PostmanifyOptions } from './postmanifyTypes';
declare const postmanifiy: (postmanCollection: PostmanCollection, opts?: PostmanifyOptions) => PostmanifyAPI;
export default postmanifiy;
