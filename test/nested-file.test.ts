import assert from 'assert';
import nock from 'nock';
import postmanify from '../index';
import { PostmanifyAPI } from '../postmanifyTypes';
import collection from './postmanify_test_api.postman_collection.json';

let api: PostmanifyAPI;

describe('Supports a nested file struture in postman', () => {
  before(() => {
    api = postmanify(collection, { urlVariables: { baseUrl: 'http://localhost' } });
  });

  it('successfully GETs from a basic URL', async () => {
    const scope = nock('http://localhost')
      .get('/game')
      .reply(200);
    const response = await api.nested.game.get();

    assert.equal(response.status, 200);
  });

  it('successfully passes query parameters through a GET call', async () => {
    const scope = nock('http://localhost')
      .get('/game')
      .query(true)
      .reply(200, (uri) => {
        return {
          uri,
        };
      });
    const response = await api.nested.game.get({
      query: { name: 'hi' },
    });
    assert.equal(response.data.uri, '/game?name=hi');
  });

  it('successfully passes the body into a POST request', async () => {
    const scope = nock('http://localhost')
      .post('/game')
      .reply(200, (uri, requestBody) => {
        return {
          requestBody,
          uri,
        };
      });

    const response = await api.nested.game.post({
      body: { test: true },
    });
    assert.equal(response.data.requestBody.test, true);
  });

  it('successfully replaces global postman variables with instantiated options', async () => {
    const scope = nock('http://localhost')
      .get('/game')
      .reply(200, (uri) => {
        return {
          uri,
        };
      });

    const response = await api.nested.game.get();
    assert.equal(response.data.uri, '/game');
  });

  it('successfully sends a PUT request', async () => {
    const scope = nock('http://localhost')
      .put(/\/game\/\d/)
      .reply(200, (uri) => {
        return {
          uri,
        };
      });

    const response = await api.nested.game.put({
      path: { gid: 1 },
    });
    assert.equal(response.data.uri, '/game/1');
  });

  it('successfully sends a PATCH request', async () => {
    const scope = nock('http://localhost')
      .patch(/\/game\/\d/)
      .reply(200, (uri) => {
        return {
          uri,
        };
      });

    const response = await api.nested.game.patch({
      path: { gid: 1 },
    });
    assert.equal(response.data.uri, '/game/1');
  });

  it('successfully applys custom headers', async () => {
    const scope = nock('http://localhost')
      .get(/\/game\/\d\/character/)
      .reply(200, function (uri, requestBody) {
        return {
          uri,
          ...this.req.headers,
        };
      });

    const response = await api.nested.character.get({
      headers: { 'x-test-header': 10 },
      path: { gid: 1 },
    });
    assert.equal(response.data['x-test-header'], 10);
  });

  it('successfully applys path variables', async () => {
    const scope = nock('http://localhost')
      .put(/\/game\/\d/)
      .reply(200, (uri) => {
        return {
          uri,
        };
      });

    const response = await api.nested.game.put({
      path: { gid: 1 },
    });
    assert.equal(response.data.uri, '/game/1');
  });
});
