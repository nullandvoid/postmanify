import assert from 'assert';
import nock from 'nock';
import postmanify from '../index';
import { PostmanifyAPI } from '../postmanifyTypes';
import collection from './postmanify_test_api.postman_collection.json';

let api: PostmanifyAPI;

describe('Supports passing no opts through the constructor', () => {
  before(() => {
    api = postmanify(collection);
  });

  it('successfully replaces postman variables with passed through options', async () => {
    const scope = nock('http://localhost')
      .get('/game')
      .reply(200, (uri) => {
        return {
          uri,
        };
      });

    const response = await api.flat.get_game_info({
      variables: { baseUrl: 'http://localhost' },
    });
    assert.equal(response.data.uri, '/game');
  });
});
