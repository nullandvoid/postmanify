# Postmanify

## What is this?
Postmanify is a tool designed to make it easier to interface with your API. It takes a postman collection and parses it to create an object that can be used to access any API endpoint that exists within the postman collection.

### What is a postman collection?
Inside the [postman app](https://www.postman.com/), you can create and share api collections. Those collections are a group of folders and endpoints that pertain to a single api. You can provide URLs, path variables, headers, tests, and the method type for each endpoint. _This module only works with postman collection v2.1_

### Why use postmanify?
Generally, postman was just used for testing directly from it's own client, but by using postmanify you can just maintain one source of truth for the api, rather than both on postman and in your code.

## How do I use postmanify?
Postmanfiy comes with a default export available for use in your code. You can access it via the require or import syntax.
```
const postmanify = require('postmanify');
import postmanify from 'postmanify';
```

The creating of the object is done by calling the required variable and passing the reference to the postman collection as well as any options you may have.
```
const api = postmanify(postman_collection, {
  urlVariable: {
    // these would be any key, value pairs for any globally used variables in your URL for postman
    baseUrl: 'http://localhost',
  },
  middleware: (middlewareOpts: { [key: string]: any }, res: AxiosResponse | undefined, error?: AxiosError) => {
    // this is an optional middleware method so you can alter data before it makes it back to your code
    // middleware opts are provided through the api call via the middlewareOpts option
    // the res is an AxiosResponse if the call is successful
    // the error is an AxiosError if the call fails
  },
});
```

This will create a variable, `api`, that gives you access to the full postman_collection in json form. In the case that you want to make a call from it, you will do it like so:
```
await api.game.post({
  body: {
    // this is where you'd put your body to pass through the api
    test: true,
  },
  headers: {
    // any option headers you have go here
    'x-test-header': 1,
  },
  path: {
    // any path variables you have go here (:gameid)
    gameid: 1,
  },
  query: {
    // any query parameters go here
    sort: 'asc',
  },
  middlewareOpts: {
    // any options you want to pass to your middleware object can be set here
  },
});
```

The naming convention that postmanify uses to generate the api object is folder name, all lowercase and any spaces changed to _, then method type for api calls. i.e. `game.get()`. In the case that you have two or more functions that use the same method type, the name of the call will be used, all lowercase and any spaces changed to _. i.e. `game.get_game_info()`.