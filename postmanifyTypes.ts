import { AxiosError, AxiosResponse } from 'axios';

export interface PostmanCollection {
  info: PostmanInfo;
  item: PostmanItem[];
  protocolProfileBehavior?: { [key: string]: any };
}

export interface PostmanifyAPI {
  [key: string]: ApiItem;
}

export interface ApiItem {
  [key: string]: any;
}

export interface ApiUrl {
  raw: string;
  queries: string[];
  variables: string[];
}

export interface ApiNode {
  url: PostmanURL| string;
  name: string;
  method: string;
}

export interface MethodOptions {
  body: { [key: string]: any };
  headers: { [key: string]: any };
  path: { [key: string]: any };
  query: { [key: string]: any };
  variables: { [key: string]: any };
  middlewareOpts: { [key: string]: any };
}

export interface PostmanifyOptions {
  urlVariables?: { [key: string]: string };
  middleware?: (middlewareOpts: { [key: string]: any }, res: AxiosResponse<any> | undefined, error?: AxiosError) => any;
}

interface PostmanInfo {
  _postman_id: string;
  name: string;
  schema: string;
}

export interface PostmanItem {
  name: string;
  item?: PostmanItem[];
  request?: PostmanRequest;
  response?: any;
  _postman_isSubFolder?: boolean;
  protocolProfileBehavior?: any;
  event?: any;
}

interface PostmanRequest {
  method: 'GET' | 'PUT' | 'DELETE' | 'POST' | 'PATCH' | 'OPTIONS' | string;
  header: PostmanHeader[];
  url: PostmanURL;
  description?: string;
  body?: any;
}

export interface PostmanURL {
  raw: string;
  protocol?: string;
  path: string[];
  variable?: PostmanVariable[];
  query?: PostmanQuery[];
  host?: string[];
  port?: string;
}

interface PostmanHeader {
  key: string;
  value: string;
  description?: string;
  name?: string;
  type?: string;
}

export interface PostmanVariable {
  id?: string;
  key: string;
  value: string;
  type?: string;
  description?: string;
}

export interface PostmanQuery {
  key: string;
  value?: string;
  description?: string;
  disabled?: boolean;
}
