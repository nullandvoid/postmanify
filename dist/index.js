"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (g && (g = 0, op[0] && (_ = 0)), _) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var axios_1 = __importDefault(require("axios"));
var urlVariables = {};
var middleware;
var postmanifiy = function (postmanCollection, opts) {
    urlVariables = opts === null || opts === void 0 ? void 0 : opts.urlVariables;
    middleware = opts === null || opts === void 0 ? void 0 : opts.middleware;
    return generateApiTree(postmanCollection.item);
};
var generateApiTree = function (items) {
    var node = {};
    var counter = {};
    var requestDetails = {};
    items.forEach(function (item) {
        if (item.item) {
            node[item.name.toLowerCase().replace(/\s/, '_')] = generateApiTree(item.item);
        }
        else if (item.request) {
            var name_1 = item.name.toLowerCase();
            name_1 = name_1.replace(/\s/g, '_');
            var request = {
                method: item.request.method.toLowerCase(),
                name: name_1,
                url: item.request ? item.request.url : '',
            };
            if (counter[item.request.method.toLowerCase()] === 1) {
                var previousItem = requestDetails[item.request.method.toLowerCase()];
                node[previousItem.name] = functionGenerator(previousItem);
                delete node[item.request.method.toLowerCase()];
                node[request.name] = functionGenerator(request);
            }
            else {
                node[item.request.method.toLowerCase()] = functionGenerator(request);
                requestDetails[item.request.method.toLowerCase()] = request;
                counter[item.request.method.toLowerCase()] = 1;
            }
        }
    });
    return node;
};
var parseUrl = function (url, methodOptions) {
    var parsed = url.raw;
    var questionMarkPosition = parsed.indexOf('?');
    parsed = parsed.substring(0, questionMarkPosition != -1 ? questionMarkPosition : parsed.length);
    var variableMatches = parsed.match(/{{([^}]+)}}/g);
    var pathMatches = parsed.match(/\/:([^/]+)\/?/g);
    variableMatches === null || variableMatches === void 0 ? void 0 : variableMatches.forEach(function (variable) {
        var _a;
        var key = variable.replace(/{{/, '').replace(/}}/, '');
        var regex = new RegExp("{{".concat(key, "}}"));
        if ((_a = methodOptions === null || methodOptions === void 0 ? void 0 : methodOptions.variables) === null || _a === void 0 ? void 0 : _a[key]) {
            parsed = parsed.replace(regex, methodOptions.variables[key]);
        }
        else if (urlVariables === null || urlVariables === void 0 ? void 0 : urlVariables[key]) {
            parsed = parsed.replace(regex, urlVariables[key]);
        }
        else {
            throw new Error("[Postmanify]: Postman Variable not supplied ".concat(key));
        }
    });
    pathMatches === null || pathMatches === void 0 ? void 0 : pathMatches.forEach(function (variable) {
        var _a;
        var key = variable.replace(/\/:/, '').replace(/\//, '');
        var regex = new RegExp("/:".concat(key));
        if ((_a = methodOptions === null || methodOptions === void 0 ? void 0 : methodOptions.path) === null || _a === void 0 ? void 0 : _a[key]) {
            parsed = parsed.replace(regex, "/".concat(methodOptions.path[key]));
        }
        else {
            throw new Error("[Postmanify]: Postman path variable not supplied ".concat(key));
        }
    });
    return parsed;
};
var functionGenerator = function (request) {
    var generatedFunction = function (methodOptions) { return __awaiter(void 0, void 0, void 0, function () {
        var returnValue, config, res, error_1;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    if (typeof request.url === 'string') {
                        throw new Error("[Postmanify]: Postman item has no URL ".concat(request.name));
                    }
                    config = {
                        headers: methodOptions === null || methodOptions === void 0 ? void 0 : methodOptions.headers,
                        params: methodOptions === null || methodOptions === void 0 ? void 0 : methodOptions.query,
                    };
                    switch (request.method) {
                        case 'post':
                            returnValue = axios_1.default.post(parseUrl(request.url, methodOptions), methodOptions === null || methodOptions === void 0 ? void 0 : methodOptions.body, config);
                            break;
                        case 'get':
                            returnValue = axios_1.default.get(parseUrl(request.url, methodOptions), config);
                            break;
                        case 'delete':
                            returnValue = axios_1.default.delete(parseUrl(request.url, methodOptions), config);
                            break;
                        case 'put':
                            returnValue = axios_1.default.put(parseUrl(request.url, methodOptions), methodOptions === null || methodOptions === void 0 ? void 0 : methodOptions.body, config);
                            break;
                        case 'patch':
                            returnValue = axios_1.default.patch(parseUrl(request.url, methodOptions), methodOptions === null || methodOptions === void 0 ? void 0 : methodOptions.body, config);
                            break;
                        default:
                            throw new Error("[Postmanify]: unsupported method type ".concat(request.method));
                    }
                    if (!middleware) return [3 /*break*/, 5];
                    res = void 0;
                    _a.label = 1;
                case 1:
                    _a.trys.push([1, 3, , 4]);
                    return [4 /*yield*/, returnValue];
                case 2:
                    res = _a.sent();
                    return [3 /*break*/, 4];
                case 3:
                    error_1 = _a.sent();
                    return [2 /*return*/, middleware(methodOptions === null || methodOptions === void 0 ? void 0 : methodOptions.middlewareOpts, undefined, error_1)];
                case 4: return [2 /*return*/, middleware(methodOptions === null || methodOptions === void 0 ? void 0 : methodOptions.middlewareOpts, res)];
                case 5: return [2 /*return*/, returnValue];
            }
        });
    }); };
    return generatedFunction;
};
exports.default = postmanifiy;
//# sourceMappingURL=index.js.map