import assert from 'assert';
import { AxiosError, AxiosResponse } from 'axios';
import nock from 'nock';
import postmanify from '../index';
import { PostmanifyAPI } from '../postmanifyTypes';
import collection from './postmanify_test_api.postman_collection.json';

let api: PostmanifyAPI;

describe('Supports custom middleware', () => {
  before(() => {
    api = postmanify(collection, {
      middleware: (middlewareOpts: { [key: string]: any }, res: AxiosResponse | undefined, error?: AxiosError) => {
        if (error) {
          return false;
        }

        if (middlewareOpts?.ignore) {
          return res;
        }

        if (res) {
          return res.data;
        }
      },
      urlVariables: {
        baseUrl: 'http://localhost',
      },
    });
  });

  it('successfully uses middleware on successful call', async () => {
    const scope = nock('http://localhost')
      .post('/game')
      .reply(200, (uri, requestBody) => {
        return {
          requestBody,
          uri,
        };
      });

    const response = await api.nested.game.post({
      body: { test: true },
    });

    assert.equal(response.requestBody.test, true);
  });

  it('successfully uses middleware on failed call', async () => {
    const scope = nock('http://localhost')
      .post('/game')
      .reply(400);

    const response = await api.nested.game.post({
      body: { test: true },
    });

    assert.equal(response, false);
  });

  it('successfully passes middleware object', async () => {
    const scope = nock('http://localhost')
      .post('/game')
      .reply(200, (uri, requestBody) => {
        return {
          requestBody,
          uri,
        };
      });

    const response = await api.nested.game.post({
      body: { test: true },
      middlewareOpts: {
        ignore: true,
      },
    });

    assert.equal(response.data.requestBody.test, true);
  });
});
